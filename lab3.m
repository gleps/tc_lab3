clear all;

fs = 1000;

%% simulation
sim('fltr');
Nfft = 2^nextpow2(length(tout));
f = (0:Nfft/2-1)/Nfft*fs;

%% plotting time diagrams
figure;
img = plot(tout(1:400), sig_raw(1:400));
xlabel('time');
ylabel('magnitude');
title('raw signal');
saveas(img, 'time_raw.png');

figure;
img = plot(tout(1:400), out(1:400));
xlabel('time');
ylabel('magnitude');
title('filtered signal');
saveas(img, 'time_out.png');

%% plotting spectrums
sp_raw = abs(fft(sig_raw, Nfft))/(Nfft/2);
sp = abs(fft(out, Nfft))/(Nfft/2);

figure;
img = plot(f, sp_raw(1:Nfft/2));
xlabel('frequency');
ylabel('magnitude');
title('raw signal spectrum');
saveas(img, 'spec_raw.png');

figure;
img = plot(f, sp(1:Nfft/2));
xlabel('frequency');
ylabel('magnitude');
title('filtered signal spectrum');
saveas(img, 'spec_out.png');
